//  DriverTableViewController.swift
//  Uber
//
//  Created by Pranoti R on 2/25/18.
//  Copyright © 2018 Pranoti R. All rights reserved.

import UIKit
import FirebaseAuth
import FirebaseDatabase
import MapKit

class DriverTableViewController: UITableViewController, CLLocationManagerDelegate  {
    
    var rideRequest: [DataSnapshot] = []
    var locationManager = CLLocationManager()
    var driverLocation = CLLocationCoordinate2D() //PROPERTY TO STORE DRIVER LOCATION
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization() //update it in info.plist
        locationManager.startUpdatingLocation()
        
        
        Database.database().reference().child("RideRequests").observe(.childAdded)
        { (snapshot) in
            self.rideRequest.append(snapshot)
            self.tableView.reloadData()
        }
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { (timer) in
            self.tableView.reloadData()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coord = manager.location?.coordinate{
            driverLocation = coord
        }
    }
    
    
    @IBAction func logOutTap(_ sender: Any) {
        try? Auth.auth().signOut()
        navigationController?.dismiss(animated: true, completion: nil) //come back to main viewcontroller
    }
    
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return the number of rows
        return rideRequest.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rideRequestCell", for: indexPath)
        
        let snapshot = rideRequest[indexPath.row]
        if let rideRequestDict = snapshot.value as? [String: Any]{
            if let email = rideRequestDict["email"] as? String {
                /*pull out the latitude and longitude of the driver from the database to pass it to the above location manager fucntion in order to get the drivers location on the map*/
                if let lat = rideRequestDict["lat"] as? Double{ //riders lat in database
                    if let long = rideRequestDict["lon"] as? Double{ //riders longitude in database
                        
                        let driverCLLocation = CLLocation(latitude: driverLocation.latitude, longitude: driverLocation.longitude)
                        let riderCLLocation = CLLocation(latitude: lat, longitude: long)
                        //calculating distance between driver and rider
                        let distance = driverCLLocation.distance(from: riderCLLocation) / 1000 //for distance in km
                        let roundedDistance = round(distance * 100) / 100
                        cell.textLabel?.text = "\(email) - \(roundedDistance) km away"
                    }
                }
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let snapshot = rideRequest[indexPath.row]
        performSegue(withIdentifier: "acceptSegue", sender: snapshot)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let acceptVC = segue.destination as? AcceptRequestViewController {
            if let snapshot = sender as? DataSnapshot {
                if let rideRequestDict = snapshot.value as? [String: Any]{
                    if let email = rideRequestDict["email"] as? String {
                        //pull out the latitude and longitude of the driver from the database to
                        //pass it to the above location manager fucntion in order to get the drivers
                        //location on the map
                        if let lat = rideRequestDict["lat"] as? Double{ //riders lat in database
                            if let long = rideRequestDict["lon"] as? Double{ //riders longitude in database
                                acceptVC.requestEmail = email
                                let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                acceptVC.requestLocation = location
                                acceptVC.driverLocation = driverLocation
                            }
                        }
                    }
                }
            }
        }
        
    }
}
