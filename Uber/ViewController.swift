//
//  ViewController.swift
//  Uber
//
//  Created by Pranoti R on 2/23/18.
//  Copyright © 2018 Pranoti R. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {
    
    @IBOutlet weak var driverLabel: UILabel!
    @IBOutlet weak var riderLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var riderDriverSwitch: UISwitch!
    
    @IBOutlet weak var topButton: UIButton!
    
    @IBOutlet weak var bottomButton: UIButton!
    
    var signUpMode = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func topTapped(_ sender: Any) {
        if emailTextField.text == "" || passwordTextField.text == ""{
            displayAlert(title: "Missing Credentials", message: "Provide valid Email and Password")
        } else {
            if let email = emailTextField.text{
                if let password = passwordTextField.text {
                    if signUpMode {
                        //Signup
                        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
                            if error !=  nil{
                                self.displayAlert(title: "Error", message: error!.localizedDescription)
                            } else {
                                print("Sign up success")
                                //check whether rider or driver has signed in
                                if self.riderDriverSwitch.isOn{
                                    //driver
                                    let req = Auth.auth().currentUser?.createProfileChangeRequest()
                                    req?.displayName = "Driver"
                                    req?.commitChanges(completion: nil)
                                }else{
                                    //rider
                                    let req = Auth.auth().currentUser?.createProfileChangeRequest()
                                    req?.displayName = "Rider"
                                    req?.commitChanges(completion: nil)
                                    self.performSegue(withIdentifier: "riderSegue", sender: nil)
                                }
                                self.performSegue(withIdentifier: "riderSegue", sender: nil)
                            }
                        })
                    } else {
                        //Login
                        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
                            if error !=  nil{
                                self.displayAlert(title: "Error", message: error!.localizedDescription)
                            } else {
                                if user?.displayName == "Driver"{
                                    //Driver
                                    //print("Driver")
                                    self.performSegue(withIdentifier: "driverSegue", sender: nil)
                                }else {
                                    //Rider
                                    self.performSegue(withIdentifier: "riderSegue", sender: nil)
                                }
                            }
                    })
                    }
                }
            }
        }
    }
    
    func displayAlert(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func bottomTapped(_ sender: Any) {
        if signUpMode{
            topButton.setTitle("Log In", for: .normal)
            bottomButton.setTitle("Switch to Signup", for: .normal)
            riderLabel.isHidden = true
            driverLabel.isHidden = true
            riderDriverSwitch.isHidden = true
            signUpMode = false
        }
        else {
            topButton.setTitle("Sign Up", for: .normal)
            bottomButton.setTitle("Switch to Login", for: .normal)
            riderLabel.isHidden = false
            driverLabel.isHidden = false
            riderDriverSwitch.isHidden = false
            signUpMode = true
        }
    }
}

